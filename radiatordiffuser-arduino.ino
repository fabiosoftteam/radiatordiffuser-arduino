#include <Homie.h>
//#include <NTPClient.h>
#include <ESP8266WiFi.h>
//#include <WiFiUdp.h>

#define kTRUE F("true")
#define kFALSE F("false")
#define kON "on"
#define kOFF "off"

#define PIN_TEMPERATURE  A0
#define PIN_RELAY_CH1    D6

unsigned long previousMillis = 0;
const long interval = 1000 * 60; //1 min

HomieNode diffuserNode("diffuser", "switch");
HomieNode temperatureNode("temperature", "temperature");

bool diffuserOnHandler(const HomieRange& range, const String& value) {
  if (value != kTRUE && value != kFALSE) return false;
  bool on = (value == kTRUE);
  digitalWrite(PIN_RELAY_CH1, on ? HIGH : LOW);
  diffuserNode.setProperty(kON).send(value);
  Homie.getLogger() << F("Diffuser is ") << (on ? kON : kOFF) << endl;
  return true;
}

void toggleRelay() {
  bool on = digitalRead(PIN_RELAY_CH1) == HIGH;
  digitalWrite(PIN_RELAY_CH1, on ? LOW : HIGH);
  diffuserNode.setProperty(kON).send(on ? kFALSE : kTRUE);
  Homie.getLogger() << F("Switch is ") << (on ? kON : kOFF) << endl;
}

void setupHandler() {
  temperatureNode.setProperty("unit").send("c");
}

void loopHandler() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    int aRead = analogRead(PIN_TEMPERATURE);
    float tempC = ((aRead / 1024.0) * 3300) / 10;
    temperatureNode.setProperty("degrees").send(String(tempC));
    //Homie.getLogger() << F("Temp = ") << (tempC) << endl;
  }
}

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;
  pinMode(PIN_RELAY_CH1, OUTPUT);
  digitalWrite(PIN_RELAY_CH1, LOW); //off
  Homie_setFirmware("soggiorno_heater_diffuser", "1.0.0");
  Homie.setSetupFunction(setupHandler).setLoopFunction(loopHandler);
  temperatureNode.advertise("unit");
  temperatureNode.advertise("degrees");
  diffuserNode.advertise(kON).settable(diffuserOnHandler);
  Homie.setup();
}

void loop () {
  Homie.loop();
}
